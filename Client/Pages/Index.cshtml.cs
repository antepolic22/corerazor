﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BusinessLogic.Models;
using Client.Context;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;

namespace Client.Pages
{
    public class IndexModel : PageModel
    {
        public IList<TblUsers> Users { get; set; }

        private readonly DevGameEngineContext _context;

        public IndexModel(DevGameEngineContext context)
        {
            _context = context;
        }

        public async Task OnGetAsync()
        {
            //Users = await _context.TblUsers.ToListAsync();
            Users = await _context.TblUsers.ToListAsync();
        }
    }
}