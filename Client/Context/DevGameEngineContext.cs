﻿using BusinessLogic.Models;
using Microsoft.EntityFrameworkCore;

namespace Client.Context
{
    public partial class DevGameEngineContext : DbContext
    {
        public DevGameEngineContext()
        {
        }

        public DevGameEngineContext(DbContextOptions<DevGameEngineContext> options)
            : base(options)
        {
        }

        public virtual DbSet<TblRoles> TblRoles { get; set; }
        public virtual DbSet<TblUsers> TblUsers { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            //if (!optionsBuilder.IsConfigured)
            //{
            //    optionsBuilder.UseSqlServer("Server=dev-db-01;Database=Dev_GameEngine;Trusted_Connection=True;");
            //}
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("ProductVersion", "2.2.4-servicing-10062");

            modelBuilder.Entity<TblRoles>(entity =>
            {
                entity.HasKey(e => e.Roleid);

                entity.ToTable("TBL_ROLES");

                entity.Property(e => e.Roleid)
                    .HasColumnName("ROLEID")
                    .HasMaxLength(40)
                    .ValueGeneratedNever();

                entity.Property(e => e.Description)
                    .HasColumnName("DESCRIPTION")
                    .HasMaxLength(200);

                entity.Property(e => e.TranslationKey)
                    .IsRequired()
                    .HasMaxLength(100)
                    .HasDefaultValueSql("('role_no_translation')");
            });

            modelBuilder.Entity<TblUsers>(entity =>
            {
                entity.HasKey(e => e.Userid);

                entity.ToTable("TBL_USERS");

                entity.HasIndex(e => e.TechId)
                    .HasName("IX_TBL_USERS_UNQ_TechID")
                    .IsUnique()
                    .HasFilter("([TechID] IS NOT NULL)");

                entity.HasIndex(e => e.Username)
                    .HasName("TBL_USERS_UK1")
                    .IsUnique();

                entity.Property(e => e.Userid).HasColumnName("USERID");

                entity.Property(e => e.Defaultlangid)
                    .HasColumnName("DEFAULTLANGID")
                    .HasMaxLength(255);

                entity.Property(e => e.Deleted).HasColumnName("DELETED");

                entity.Property(e => e.Email)
                    .HasColumnName("EMAIL")
                    .HasMaxLength(200);

                entity.Property(e => e.Firstname)
                    .HasColumnName("FIRSTNAME")
                    .HasMaxLength(100);

                entity.Property(e => e.Lastname)
                    .HasColumnName("LASTNAME")
                    .HasMaxLength(100);

                entity.Property(e => e.Operationcode)
                    .HasColumnName("OPERATIONCODE")
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.PasswordDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.PersonalId).HasMaxLength(50);

                entity.Property(e => e.Pin)
                    .HasColumnName("PIN")
                    .HasMaxLength(96)
                    .IsUnicode(false);

                entity.Property(e => e.TanTable).HasMaxLength(1000);

                entity.Property(e => e.TechId)
                    .HasColumnName("TechID")
                    .HasMaxLength(200);

                entity.Property(e => e.Transnoticesend).HasColumnName("TRANSNOTICESEND");

                entity.Property(e => e.UsedPasswords).HasMaxLength(1000);

                entity.Property(e => e.Username)
                    .HasColumnName("USERNAME")
                    .HasMaxLength(100);

                entity.Property(e => e.Userpass)
                    .HasColumnName("USERPASS")
                    .HasMaxLength(120);
            });

            modelBuilder.HasSequence("tdba_log_seq").StartsAt(1000);
        }
    }
}