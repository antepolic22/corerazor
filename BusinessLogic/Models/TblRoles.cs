﻿namespace BusinessLogic.Models
{
    public partial class TblRoles
    {
        public string Roleid { get; set; }
        public string Description { get; set; }
        public string TranslationKey { get; set; }
        public bool IsHidden { get; set; }
    }
}
