﻿using System;

namespace BusinessLogic.Models
{
    public partial class TblUsers
    {
        public int Userid { get; set; }
        public string Username { get; set; }
        public string Userpass { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Email { get; set; }
        public string Pin { get; set; }
        public byte Deleted { get; set; }
        public string Operationcode { get; set; }
        public DateTime PasswordDate { get; set; }
        public string UsedPasswords { get; set; }
        public int FailedCount { get; set; }
        public int? CardId { get; set; }
        public string TanTable { get; set; }
        public int? PartnerId { get; set; }
        public int Transnoticesend { get; set; }
        public string Defaultlangid { get; set; }
        public string TechId { get; set; }
        public string PersonalId { get; set; }
    }
}